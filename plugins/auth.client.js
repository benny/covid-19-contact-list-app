const jwt = require('jsonwebtoken')

export default function ({
  env,
  store
}) {
  // refresh token every 60 min
  if (env.autoRefresh) {
    setInterval(() => {
      store.dispatch('auth/refreshToken')
    }, (env.refreshInterval || 60) * 60 * 1000)
  }

  if (localStorage.getItem('authToken')) {
    // Check if token is expired
    if ((new Date().getTime() / 1000) < jwt.decode(localStorage.getItem('authToken')).exp) {
      // Token is still valid
      store.dispatch('auth/storeToken', localStorage.getItem('authToken')).then(() => {
        // refresh token
        store.dispatch('auth/refreshToken')
      })
    } else {
      // Token is expired, remove token from local storage
      localStorage.removeItem('authToken')
    }
  }
}
