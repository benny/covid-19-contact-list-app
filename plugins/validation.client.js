export default function (context, inject) {
  const rules = {
    required: value => !!value || 'Dies ist ein Pflichtfeld!',
    email: (value) => {
      const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
      return pattern.test(value) || 'Ungültige E-Mail Adresse.'
    },
    passwordMinLength: value => (value || '').length >= 8 || 'Das Password muss min. 8 Zeichen lang sein.',
    passphraseMinLength: value => (value || '').length >= 8 || 'Das Passphrase muss min. 8 Zeichen lang sein.',
    zipcode: (value) => {
      const pattern = /^[0-9]{5}$/
      return pattern.test(value) || 'Ungültige Postleitzahl.'
    },
    housenumber: (value) => {
      const pattern = /^[\d-]+[\s]?[a-zA-Z]?$/
      return pattern.test(value) || 'Ungültige Hausnummer.'
    },
    cssProtection: (value) => {
      const pattern = /^[^<>\\/{}"']*$/
      return pattern.test(value) || 'Nicht erlaubte Zeichen (<>\\/{}"\').'
    }
  }
  // Inject to context as $api
  inject('validation', { rules })
}
