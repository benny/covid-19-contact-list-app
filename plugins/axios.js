export default function ({
  $axios,
  store,
  redirect
}, inject) {
  const api = $axios.create({
    headers: {
      common: {
        Accept: 'application/json'
      }
    }
  })
  api.onRequest(() => {
    store.dispatch('showSpinner')
  })
  api.onError((error) => {
    store.dispatch('hideSpinner')
    if (error.response && error.response.status === 401 && store.getters['auth/isAuthenticated']) {
      // Token is expired
      store.dispatch('auth/removeToken').then(() => redirect('/login'))
    }
  })
  api.onResponse(() => {
    store.dispatch('hideSpinner')
  })
  // Inject to context as $api
  inject('api', api)
}
