export default function (context, inject) {
  const arrayBufferToBase64 = (buffer) => {
    let binary = ''
    const bytes = new Uint8Array(buffer)
    const len = bytes.byteLength
    for (let i = 0; i < len; i++) {
      binary += String.fromCharCode(bytes[i])
    }
    return btoa(binary)
  }
  const base64ToArrayBuffer = (base64) => {
    const binaryString = atob(base64)
    const len = binaryString.length
    // eslint-disable-next-line prefer-const
    let bytes = new Uint8Array(len)
    for (let i = 0; i < len; i++) {
      bytes[i] = binaryString.charCodeAt(i)
    }
    return bytes.buffer
  }
  const rsaAlg = {
    name: 'RSA-OAEP',
    modulusLength: 4096,
    publicExponent: new Uint8Array([1, 0, 1]),
    hash: 'SHA-256'
  }
  const generateKeypair = async () => {
    return await crypto.subtle.generateKey(
      rsaAlg,
      true,
      ['encrypt', 'decrypt']
    )
  }
  const exportKey = async (key) => {
    return await crypto.subtle.exportKey('jwk', key)
  }
  const importKey = async (key, usages) => {
    return await crypto.subtle.importKey(
      'jwk',
      key,
      rsaAlg,
      false,
      usages
    )
  }
  const encrypt = async (key, plaintext) => {
    const ptUint8 = new TextEncoder().encode(plaintext)
    const ctBuffer = await crypto.subtle.encrypt(rsaAlg, key, ptUint8)
    return arrayBufferToBase64(ctBuffer)
  }
  const decrypt = async (key, ciphertext) => {
    const plainBuffer = await crypto.subtle.decrypt(rsaAlg, key, base64ToArrayBuffer(ciphertext))
    const plaintext = new TextDecoder().decode(plainBuffer)

    return plaintext
  }
  const encryptKey = async (privateKey, passphrase) => {
    const jwk = await exportKey(privateKey)
    const string = JSON.stringify(jwk)

    const pwUtf8 = new TextEncoder().encode(passphrase)
    const pwHash = await crypto.subtle.digest('SHA-256', pwUtf8)

    const iv = crypto.getRandomValues(new Uint8Array(12))

    const alg = {
      name: 'AES-GCM',
      iv
    }

    const key = await crypto.subtle.importKey('raw', pwHash, alg, false, ['encrypt'])

    const ptUint8 = new TextEncoder().encode(string)
    const ctBuffer = await crypto.subtle.encrypt(alg, key, ptUint8)

    const ctArray = Array.from(new Uint8Array(ctBuffer))
    const ctStr = ctArray.map(byte => String.fromCharCode(byte)).join('')
    const ctBase64 = btoa(ctStr)

    const ivHex = Array.from(iv).map(b => ('00' + b.toString(16)).slice(-2)).join('')

    return ivHex + ctBase64
  }
  const decryptKey = async (encryptedPrivateKey, passphrase) => {
    const pwUtf8 = new TextEncoder().encode(passphrase)
    const pwHash = await crypto.subtle.digest('SHA-256', pwUtf8)

    const iv = encryptedPrivateKey.slice(0, 24).match(/.{2}/g).map(byte => parseInt(byte, 16))

    const alg = {
      name: 'AES-GCM',
      iv: new Uint8Array(iv)
    }

    const key = await crypto.subtle.importKey('raw', pwHash, alg, false, ['decrypt'])

    const ctStr = atob(encryptedPrivateKey.slice(24))
    const ctUint8 = new Uint8Array(ctStr.match(/[\s\S]/g).map(ch => ch.charCodeAt(0)))

    const plainBuffer = await crypto.subtle.decrypt(alg, key, ctUint8)
    const plaintext = new TextDecoder().decode(plainBuffer)

    return JSON.parse(plaintext)
  }
  inject('crypto', {
    generateKeypair,
    encryptKey,
    decryptKey,
    exportKey,
    importKey,
    encrypt,
    decrypt
  })
}
