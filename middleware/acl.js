const routeOption = (route, value) => {
  return route.matched.some((m) => {
    if (process.client) {
      // Client
      return Object.values(m.components).some(
        component => component.options && component.options.acl === value
      )
    } else {
      // SSR
      return Object.values(m.components).some(component =>
        Object.values(component._Ctor).some(
          ctor => ctor.options && ctor.options.acl === value
        )
      )
    }
  })
}

export default function ({
  store,
  redirect,
  route,
  error
}) {
  if (routeOption(route, false)) {
    // no Auth and ACL for this page
  } else if (routeOption(route, 'guest')) {
    // only guest users
    if (store.getters['auth/isAuthenticated'] === true) {
      // user is logged in, show error
      return error(new Error('Diese Seite kann nur als Gast aufgerufen werden!'))
    }
  } else if (store.getters['auth/isAuthenticated'] === false) {
    // only auth users, redirect to login
    return redirect('/login')
  }
}
