export const state = () => ({
  profil: null
})

export const mutations = {
  setCustomer (state, value) {
    state.profil = value
  },
  setPublicKey (state, value) {
    state.profil.publicKey = value
  },
  setEncyptedPrivateKey (state, value) {
    state.profil.encyptedPrivateKey = value
  }
}
export const actions = {
  async fetchCustomer ({ commit }) {
    const customer = await this.$api.$get('/api/customers/me')
    commit('setCustomer', customer)
  }
}
export const getters = {
  customerProfile (state) {
    return state.profil
  }
}
