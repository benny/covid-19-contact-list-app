export const state = () => ({
  spinner: false
})

export const mutations = {
  setSpinner (state, value) {
    state.spinner = value
  }
}
export const actions = {
  showSpinner (state) {
    state.commit('setSpinner', true)
  },
  hideSpinner (state) {
    state.commit('setSpinner', false)
  }
}
