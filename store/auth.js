export const state = () => ({
  user: null,
  token: null
})

export const mutations = {
  setUser (state, value) {
    state.user = value
  },
  setToken (state, value) {
    state.token = value
  },
  unsetToken (state) {
    state.token = null
    state.user = {}
  }
}

export const actions = {
  storeToken ({ dispatch, commit }, token) {
    commit('setToken', token)
    this.$api.setToken(token, 'Bearer')
    localStorage.setItem('authToken', token)
    dispatch('fetchUser')
  },
  async fetchUser ({ dispatch, commit }) {
    const user = await this.$api.$get('/api/users/me')
    commit('setUser', user)
    dispatch('customer/fetchCustomer', {}, {
      root: true
    })
  },
  removeToken ({ commit }) {
    localStorage.removeItem('authToken')
    commit('setToken', null)
    commit('setUser', null)
    this.$api.setToken(false)
    commit('customer/setCustomer', null, {
      root: true
    })
  },
  async refreshToken ({ state, commit }) {
    if (state.token !== null) {
      const { token } = await this.$api.$get('/api/users/refreshtoken')
      commit('setToken', token)
      this.$api.setToken(token, 'Bearer')
      localStorage.setItem('authToken', token)
    }
  }
}

export const getters = {
  isAuthenticated (state) {
    return state.token !== null
  },
  loggedInUser (state) {
    return state.user
  }
}
