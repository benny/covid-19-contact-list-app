# covid-19-contact-list-app

> App to collect the contact data of guests

## Build Setup

```bash
# install dependencies
$ npm install

# install now cli
$ npm install -g now@18
# Version 19 isn't working well

# serve with hot reload at localhost:3000
$ now dev

# build preview
$ now