'use strict';
const Customer = require('../../_models/customer');
const User = require('../../_models/user');
const app = require('../../app')
const auth = require('../../_utils/authentication')
const acl = require('../../_utils/acl')
const duplicateKeyError = require('../../_utils/errors').duplicateKeyError;
const notFound = require('../../_utils/errors').notFound;
const Schemas = require('../../_utils/schemas')
const validationMiddleware = require('../../_utils/validationMiddleware')

let conn = null

app.post("/api/customers/register", acl.authorize, validationMiddleware(Schemas.registration), async (req, res, next) => {
  if (conn == null) {
    conn = require('../../_utils/db-connect')
  }
  try {
    var customer = await Customer.create({
      companyname: req.body.companyname,
      street: req.body.street,
      housenumber: req.body.housenumber,
      zipcode: req.body.zipcode,
      city: req.body.city,
      email: req.body.email,
    })
  } catch (err) {
    return next(err)
  }

  try {
    var user = await User.create({
      email: req.body.email,
      password: req.body.password,
      customer: customer._id,
      role: 'owner'
    })
    var token = await user.generateToken()
  } catch (err) {
    await customer.remove()

    if (err.code == 11000) {
      return next(new duplicateKeyError())
    } else {
      return next(err)
    }
  }

  try {
    var token = await user.generateToken()
  } catch (err) {
    await customer.remove()
    await user.remove()

    return next(err)
  }
  return res.status(201).json({
    token: token
  });
})

app.get("/api/customers/me", auth.authenticate, acl.authorize, (req, res, next) => {
  if (conn == null) {
    conn = require('../../_utils/db-connect')
  }
  Customer.findById(req.user.customer).then(function (customer) {
    if (!customer) {
      return next(new notFound("The requested resource " + req.user.customer + " couldn't be found"));
    } else {

      return res.json(customer);
    }
  }).catch(function (err) {
    if (err.name == "CastError" && err.path == "_id") {
      return next(new notFound("The requested resource " + req.user.customer + " couldn't be found"));
    } else {
      return next(err);
    }
  })
})

app.get("/api/customers/profile/:id", acl.authorize, (req, res, next) => {
  if (conn == null) {
    conn = require('../../_utils/db-connect')
  }
  Customer.findById(req.params.id).then(function (customer) {
    if (!customer) {
      return next(new notFound("The requested resource " + req.params.id + " couldn't be found"));
    } else {
      return res.json({
        companyname: customer.companyname,
        publicKey: customer.publicKey
      });
    }
  }).catch(function (err) {
    if (err.name == "CastError" && err.path == "_id") {
      return next(new notFound("The requested resource " + req.params.id + " couldn't be found"));
    } else {
      return next(err);
    }
  })
})

require('../../_utils/errorhandler')(app);
module.exports = app