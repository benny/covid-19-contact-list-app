'use strict';
const Guest = require('../../_models/guest');
const app = require('../../app')
const auth = require('../../_utils/authentication')
const acl = require('../../_utils/acl')

let conn = null

app.get("/api/customers/guests", auth.authenticate, acl.authorize, (req, res, next) => {
  if (conn == null) {
    conn = require('../../_utils/db-connect')
  }
  Guest.find({ customer: req.user.customer}).then(function (guests) {
    return res.json(guests);
  }).catch(next)
})

require('../../_utils/errorhandler')(app);
module.exports = app