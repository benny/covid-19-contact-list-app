'use strict';
const Guest = require('../../_models/guest');
const app = require('../../app')
const acl = require('../../_utils/acl')
const Schemas = require('../../_utils/schemas')
const validationMiddleware = require('../../_utils/validationMiddleware')

let conn = null

app.post("/api/guests/:customerId", acl.authorize, validationMiddleware(Schemas.guestRegistration), async (req, res, next) => {
  if (conn == null) {
    conn = require('../../_utils/db-connect')
  }
  try {
    var guest = await Guest.create({
      customer: req.params.customerId,
      firstname: req.body.firstname,
      lastname: req.body.lastname,
      street: req.body.street,
      housenumber: req.body.housenumber,
      zipcode: req.body.zipcode,
      city: req.body.city
    })
  } catch (err) {
    return next(err)
  }
  var token = await guest.generateToken()
  return res.status(201).json({
    'accessToken': token
  });
})

require('../../_utils/errorhandler')(app);
module.exports = app