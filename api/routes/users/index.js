'use strict';
const User = require('../../_models/user');
const app = require('../../app')
const auth = require('../../_utils/authentication')
const acl = require('../../_utils/acl')
const authenticationError = require('../../_utils/errors').authenticationError;
const notFound = require('../../_utils/errors').notFound;
const Schemas = require('../../_utils/schemas')
const validationMiddleware = require('../../_utils/validationMiddleware')

let conn = null

app.post("/api/users/authenticate", acl.authorize, validationMiddleware(Schemas.login), (req, res, next) => {
  if (conn == null) {
    conn = require('../../_utils/db-connect')
  }
  User.findOne({
    email: String(req.body.email)
  }, function (err, user) {
    if (err) throw err;

    if (!user) {
      return next(new authenticationError())
    } else {
      // Check if password matches
      user.comparePassword(String(req.body.password))
        .then(function () {
          return user.generateToken()
        })
        .then(function (token) {
          res.json({
            token: token
          });
        })
        .catch(function (err) {
          return next(new authenticationError())
        })
    }
  });
})

app.get("/api/users/me", auth.authenticate, acl.authorize, (req, res, next) => {
  if (conn == null) {
    conn = require('../../_utils/db-connect')
  }
  User.findById(req.user.id, {
    "password": 0
  }).then(function (user) {
    if (!user) {
      return next(new notFound("The requested resource " + req.user.id + " couldn't be found"));
    } else {

      return res.json(user);
    }
  }).catch(function (err) {
    if (err.name == "CastError" && err.path == "_id") {
      return next(new notFound("The requested resource " + req.user.id + " couldn't be found"));
    } else {
      return next(err);
    }
  });
})

app.get("/api/users/refreshtoken", auth.authenticate, acl.authorize, (req, res, next) => {
  if (conn == null) {
    conn = require('../../_utils/db-connect')
  }
  User.findById(req.user.id).then(function (user) {
    if (!user) {
      return next(new notFound("The requested resource " + req.params.id + " couldn't be found"));
    } else {
      user.generateToken()
        .then(function (token) {
          res.json({
            token: token
          });
        })
        .catch(function (err) {
          return next(err)
        })
    }
  }).catch(function (err) {
    if (err.name == "CastError" && err.path == "_id") {
      return next(new notFound("The requested resource " + req.params.id + " couldn't be found"));
    } else {
      return next(err);
    }
  });
})

require('../../_utils/errorhandler')(app);
module.exports = app