const Joi = require('joi')

const regexCSS = new RegExp(/^[^<>\\/{}"']*$/)
const regexZipCode = new RegExp(/^[0-9]{5}$/)
const regexHousenumber = new RegExp(/^[\d-]+[\s]?[a-zA-Z]?$/)
const regexBase64 = new RegExp(/^(?:[A-Za-z0-9+/]{4})*(?:[A-Za-z0-9+/]{2}==|[A-Za-z0-9+/]{3}=)?$/)

// Fields
const passwordField = Joi.string().trim().required().min(8)
const emailField = Joi.string().trim().required().email()
const companynameField = Joi.string().trim().required().regex(regexCSS)
const streetField = Joi.string().trim().required().regex(regexCSS)
const housenumberField = Joi.string().trim().required().regex(regexHousenumber).regex(regexCSS)
const zipcodeField = Joi.string().trim().required().regex(regexZipCode)
const cityField = Joi.string().trim().required().regex(regexCSS)
const encryptedField = Joi.string().trim().required().regex(regexBase64)
const publicKey = Joi.object().keys({
  alg: Joi.string().trim().required(),
  e: Joi.string().trim().required(),
  ext: Joi.boolean().required(),
  key_ops: Joi.array().required(),
  kty: Joi.string().trim().required(),
  n: Joi.string().trim().required()
})

// Forms
const schemaRegistration = Joi.object().keys({
  companyname: companynameField,
  street: streetField,
  housenumber: housenumberField,
  zipcode: zipcodeField,
  city: cityField,
  email: emailField,
  password: passwordField,
  password2: passwordField.valid(Joi.ref('password')).options({
    language: {
      any: {
        allowOnly: 'Passwords do not match',
      }
    }
  }),
  publicKey: publicKey,
  encyptedPrivateKey: encryptedField
});
const schemaGuestRegistration = Joi.object().keys({
  firstname: encryptedField,
  lastname: encryptedField,
  street: encryptedField,
  housenumber: encryptedField,
  zipcode: encryptedField,
  city: encryptedField
});
const schemaLogin = Joi.object().keys({
  email: emailField,
  password: passwordField
});

module.exports = {
  registration: schemaRegistration,
  guestRegistration: schemaGuestRegistration,
  login: schemaLogin
}