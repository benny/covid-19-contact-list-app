const Joi = require('joi');
const {
  validationError
} = require('./errors')


const middleware = (schema, property) => {
  return (req, res, next) => {
    const {
      error
    } = Joi.validate(req.body, schema);

    if (error === null) {
      next();
    } else {
      const { details } = error;
      const message = details.map(i => i.message).join(',');

      return next(new validationError(message))
    }
  }
}
module.exports = middleware;
