'use strict';

const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const User = require('../_models/user');
const config = require('../_config/environment');
const authenticationError = require('./errors').authenticationError;
const unauthorizedError = require('./errors').unauthorizedError;

let conn = null

module.exports = {
  initialize: () => passport.initialize(),
  authenticate: function (req, res, next) {
    return passport.authenticate(['jwt'], {
      session: false
    }, function (err, user) {
      if (err) {
        return next(err);
      }
      if (!user) {
        return next(new unauthorizedError())
      }
      //success
      req.logIn(user, function (err) {
        if (err) {
          return next(err);
        }
        return next();
      });
    })(req, res, next)
  },
  setJwtStrategy: function () {
    var opts = {};
    opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();
    opts.secretOrKey = config.jwt.secret;
    opts.issuer = config.jwt.issuer;
    passport.use(new JwtStrategy(opts, function (jwt_payload, done) {
      if (conn == null) {
        conn = require('./db-connect')
      }
      User.findOne({
        _id: jwt_payload.id
      }, function (err, user) { // needed for the case that the user was locked in the meantime
        if (err) {
          return done(err, false);
        }
        if (user) {
          done(null, user);
        } else {
          done(null, false);
        }
      });
    }));
  }
};
passport.serializeUser(function (user, done) {
  done(null, user);
});

passport.deserializeUser(function (user, done) {
  done(null, user);
});