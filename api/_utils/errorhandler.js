'use strict';

const notFound = require('./errors').notFound;

module.exports = function (app) {
  // All undefined asset or api routes should return a 404
  app.all('/*', function (req, res, next) {
     next(new notFound());
  });

  app.use(async function (err, req, res, next) {
    console.error(JSON.stringify(err))
    res.status(err.statusCode || 500);
    res.setHeader('Content-type', 'application/json')
    res.json({
      message: err.message,
      error: err
    });
  });
}