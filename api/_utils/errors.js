module.exports = {
  notFound: function (message, errorCode) {
    Error.captureStackTrace(this, this.constructor);

    this.name = this.constructor.name;
    this.message = message || 'The requested resource couldn\'t be found';
    this.statusCode = 404;
    this.errorCode = errorCode || 404;
  },
  validationError: function (message) {
    Error.captureStackTrace(this, this.constructor);

    this.name = this.constructor.name;
    this.message = message || 'payload validation faild';
    this.statusCode = 400;
    this.errorCode = 4000;
  },
  duplicateKeyError: function (message) {
    Error.captureStackTrace(this, this.constructor);

    this.name = this.constructor.name;
    this.message = message || 'Dublicate key';
    this.statusCode = 400;
    this.errorCode = 4005;
  },
  keysAlreadyStored: function (message) {
    Error.captureStackTrace(this, this.constructor);

    this.name = this.constructor.name;
    this.message = message || "The keys are already stored. It isn't allowed to store new keys.";
    this.statusCode = 400;
    this.errorCode = 4006;
  },
  unauthorizedError: function (message) {
    Error.captureStackTrace(this, this.constructor);

    this.name = this.constructor.name;
    this.message = message || 'Unauthorized';
    this.statusCode = 401;
    this.errorCode = 4001;
  },
  accessDeniedError: function (message) {
    Error.captureStackTrace(this, this.constructor);

    this.name = this.constructor.name;
    this.message = message || 'Forbidden';
    this.statusCode = 403;
    this.errorCode = 403;
  },
  authenticationError: function (message) {
    Error.captureStackTrace(this, this.constructor);

    this.name = this.constructor.name;
    this.message = message || 'Authentication failed';
    this.statusCode = 401;
    this.errorCode = 4002;
  },
  requiredError: function (message, field) {
    Error.captureStackTrace(this, this.constructor);

    this.name = this.constructor.name;
    this.message = message || 'Required value is missing';
    this.statusCode = 400;
    this.errorCode = 4004;
    if (field !== undefined) {
      this.field = field
    }
  }
};
