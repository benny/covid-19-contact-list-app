const acl = require('express-acl');

const accessDeniedError = require('./errors').accessDeniedError;

acl.config({
  baseUrl: 'api',
  filename: 'nacl.json',
  path: 'api/_utils',
  defaultRole: 'anonymous',
  decodedObjectName: 'user',
  roleSearchPath: 'user.role',
  denyCallback: (res) => {
    var err = new accessDeniedError()
    return res.status(403).json({
      message: err.message,
      error: err
    });
  }
});

module.exports = acl
