'use strict';
const mongoose = require('mongoose');
module.exports = mongoose.connect(process.env.MONGODB_URI, {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  bufferCommands: false,
  bufferMaxEntries: 0,
  connectTimeoutMS: 5000
});