'use strict';
const mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  jwt = require('jsonwebtoken'),
  config = require('../_config/environment');


var GuestSchema = new Schema({
  customer: {
    type: Schema.Types.ObjectId,
    ref: 'Customer',
    required: true
  },
  firstname: {
    type: Schema.Types.String,
    required: true
  },
  lastname: {
    type: Schema.Types.String,
    required: true
  },
  street: {
    type: Schema.Types.String,
    required: true
  },
  housenumber: {
    type: Schema.Types.String,
    required: true
  },
  zipcode: {
    type: Schema.Types.String,
    required: true
  },
  city: {
    type: Schema.Types.String,
    required: true
  },
  checkin: {
    type: Date,
    index: true,
    timezone: "Europe/Berlin",
    default: Date.now
  },
  checkout: {
    type: Date,
    timezone: "Europe/Berlin"
  }
}, {
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});

GuestSchema.methods.generateToken = function (callback) {
  var _this = this
  return new Promise(function (resolve, reject) {
    const payload = {
      id: _this._id,
    };

    const options = {
      expiresIn: config.jwt.expiration,
      issuer: config.jwt.issuer
    };

    const token = jwt.sign(payload, config.jwt.secret, options);

    return callback ? callback(null, token) : resolve(token)
  })
};

module.exports = mongoose.model('Guest', GuestSchema);