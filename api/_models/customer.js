'use strict';
var mongoose = require('mongoose'),
  Schema = mongoose.Schema;


var CustomerSchema = new Schema({
  companyname: {
    type: Schema.Types.String,
    required: true
  },
  street: {
    type: Schema.Types.String,
    required: true
  },
  housenumber: {
    type: Schema.Types.String,
    required: true
  },
  zipcode: {
    type: Schema.Types.String,
    required: true
  },
  city: {
    type: Schema.Types.String,
    required: true
  },
  email: {
    type: Schema.Types.String,
    required: true
  },
  publicKey: {
    type: Schema.Types.Mixed
  },
  encyptedPrivateKey: {
    type: Schema.Types.String
  }
}, {
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});


module.exports = mongoose.model('Customer', CustomerSchema);