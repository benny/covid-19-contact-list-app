'use strict';
const mongoose = require('mongoose'),
  Schema = mongoose.Schema,
  bcrypt = require('bcryptjs'),
  jwt = require('jsonwebtoken'),
  config = require('../_config/environment');


var UserSchema = new Schema({
  email: {
    type: Schema.Types.String,
    index: true,
    lowercase: true,
    unique: true,
    required: true,
    match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/, 'No valid email address']
  },
  password: {
    type: Schema.Types.String,
    required: true
  },
  role: {
    type: String,
    enum: ['user', 'owner', 'admin'],
    default: 'user'
  },
  customer: {
    type: Schema.Types.ObjectId,
    ref: 'Customer'
  }
}, {
  toObject: {
    virtuals: true
  },
  toJSON: {
    virtuals: true
  }
});

UserSchema.pre('save', function (next) {
  var user = this;
  if (this.isModified('password') || this.isNew) {
    bcrypt.genSalt(10, function (err, salt) {
      if (err) {
        return next(err);
      }
      bcrypt.hash(user.password, salt, function (err, hash) {
        if (err) {
          return next(err);
        }
        user.password = hash;
        next();
      });
    });
  } else {
    return next();
  }
});

UserSchema.methods.comparePassword = function (pw, callback) {
  var _this = this
  return new Promise(function (resolve, reject) {
    bcrypt.compare(pw, _this.password, function (err, isMatch) {
      if (err) {
        return callback ? callback(err, null) : reject(err)
      } else if (isMatch === false) {
        return callback ? callback(null, false) : reject(new Error("Password mismatch"))
      } else {
        return callback ? callback(null, true) : resolve()
      }
    });
  })
};

UserSchema.methods.generateToken = function (callback) {
  var _this = this
  return new Promise(function (resolve, reject) {
    const payload = {
      id: _this._id,
      //email: _this.email,
      // role: _this.role
    };

    const options = {
      expiresIn: config.jwt.expiration,
      issuer: config.jwt.issuer
    };

    const token = jwt.sign(payload, config.jwt.secret, options);

    return callback ? callback(null, token) : resolve(token)
  })
};

UserSchema.post("save", function (doc) {
  delete doc.password
});
module.exports = mongoose.model('User', UserSchema);