const express = require("express")
const helmet = require('helmet')
const app = express()
const bodyParser = require('body-parser')
const auth = require('./_utils/authentication')

console.log("ENV:", process.env.NODE_ENV)
express.static(__dirname + '/_utils/nacl.json')

app.use(helmet())
app.use(bodyParser.json({
  limit: '50mb'
}));
app.use(bodyParser.raw({
  type: 'image/*',
  limit: '2mb'
}))

app.use(auth.initialize())
auth.setJwtStrategy()

module.exports = app