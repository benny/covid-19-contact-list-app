'use strict';

var path = require('path');
var _ = require('lodash');

// All configurations will extend these options
// ============================================
var all = {
  env: process.env.NODE_ENV,

  loglevel: process.env.LOG_LEVEL || 'info',
  jwt: {
    issuer: "Covid-19-contact-list-app",
    secret: process.env.JWT_SECRET,
    expiration: "12h"
  }
};

// Export the config object based on the NODE_ENV
// ==============================================
module.exports = _.merge(
  all,
  require('./' + process.env.NODE_ENV + '.js') || {});
