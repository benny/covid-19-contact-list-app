'use strict';

// Development specific configuration
// ==================================
module.exports = {
  loglevel: process.env.LOG_LEVEL || 'debug'
};
